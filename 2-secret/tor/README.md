# Tor

Proiectul de echipă pentru curs și seminar este despre [Tor](https://www.torproject.org/index.html.en), ca arhitectură și procedură criptografică de rutare și anonimizare.

Pentru acces direct la PDF, click [aici](https://gitlab.com/adimanea/sla/blob/master/2-secret/tor/latex/tor.pdf).

Pentru acces direct la prezentare (handout), click [aici](https://gitlab.com/adimanea/sla/blob/master/2-secret/tor/beamer/torp-handout.pdf).


## Idei aplicații
### Test obținere adresă `.onion` pentru localhost
- [Ghid oficial](https://www.torproject.org/docs/tor-onion-service.html.en#one);
- [Ghid pentru Windows](https://www.makeuseof.com/tag/create-hidden-service-tor-site-set-anonymous-website-server/);
- [Ghid pentru Ubuntu](https://null-byte.wonderhowto.com/how-to/host-your-own-tor-hidden-service-with-custom-onion-address-0180159/);
- [Ghid pentru toate sistemele](https://www.comparitech.com/blog/vpn-privacy/how-to-set-up-a-tor-hidden-service/)
