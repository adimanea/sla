# Securitate și Logică Aplicată

Aici voi păstra notițele, temele, proiectele și materialele pe care le fac sau le folosesc în timpul masterului de Securitate și Logică Aplicată (SLA) de la Universitatea din București.

Directoarele sînt denumite după regula `[semestrul]-[materia]`.

De asemenea, am inclus și notițele rezultate în urma activității de [cercetare](https://gitlab.com/adimanea/sla/tree/master/cercetare).
