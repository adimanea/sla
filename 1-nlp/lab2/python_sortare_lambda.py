###############################################################################
# python_sortare_lambda.py
# a. Se considera o lista de numere. Sa se sorteze folosind o functie lambda
# asa cum s-ar fi sortat daca numerele erau siruri
# b. Se considera o lista de numere. Sa se sorteze folosind o functie lambda
# comparand intai ultima cifra apoi penultima (asa cum am fi sortat daca erau
# siruri cu literele in ordine inversa.
# c. Se considera o lista de numere. Sa se sorteze dupa lungimea numarului.
# d. Se considera o lista de numere. Sa se sorteze dupa numarul de cifre
# distincte.
# e. Se considera o lista de siruri - expresii: ["1+2+3","2-5","3+4","5*10"].
# Sa se sorteze dupa valoarea expresiei.
###############################################################################
