###############################################################################
# python_fisier_try_catch.py
# Sa se scrie o functie care citeste o matrice de numere dintr-un fisier si
# calculeaza suma lor. Randurile matricii sunt fiecare pe un rand nou. Pot
# exista randuri vide intre randurile corespunzatovre matricii. Elementele
# pe un rand sunt separate prin spatii. Functia va putea arunca urmatoarele
# erori:
#    IOError in cazul in care nu gaseste fisierul
#    ValueError in cazul in care matricea nu e formata din numere
#    Exceptie custom in cazul in care lista de liste rezultata nu e matrice
# (nu sunt toate liniile de aceeasi lungime. Cum putem testa cat mai eficient?
# Sa se apeleze functia atat pentru cazul corect cat si pentru cazurile cu
# exceptie. Functia va fi inclusa intr-un try..except si se vor afisa
# mesajele concludente pentru fiecare tip de eroare. Se vor uploada si
# fisierele de input.
###############################################################################
