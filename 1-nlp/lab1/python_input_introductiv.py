######################################################################
# Un program de un singur rînd care afișează "Cum te numești?"
# apoi, după ce utilizatorul răspunde, scrie "Salut, [nume],
# îți place laboratorul?". Dacă răspunde "da", afișăm ":)", altfel ":(".
# Dialogul se va afișa cu replicile separate prin linii.
######################################################################
q1, q2, q3 = "Cum te numești?", "Salut, ", ", îți place laboratorul?" + "\n"


print(":)") if (input(q2 + input(q1 + "\n") + q3) == "da") else print(":(")
