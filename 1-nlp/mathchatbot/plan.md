# Math Chatbot

## Plan
- aleg cîteva synset-uri de termeni matematici și le stochez într-o listă;
- parsez replica introdusă de utilizator, caut cu prioritate substantive, apoi verbe și caut fac o listă de scoruri de similaritate între substantivul respectiv și substantivele din lista stocată;
- aleg substantivul cu scorul cel mai mare de similaritate și verbul din propoziție;
- dau răspunsuri potrivite după verb:
> -- I want to *learn* about **derivatives**.
> -- To *learn* about derivatives, you should check out a book.
> -- I want to *practice* what I learned.
> -- To *practice*, you should do some exercises.
> -- I want to *read* about **limits**.
> -- You should check out a book for that.
- dialogul continuă pînă la "bye".


## Resurse
- [Chatbot Fundamentals](https://apps.worldwritable.com/tutorials/chatbot/);
- [Build a chatbot](https://moz.com/blog/chat-bot);
- [Build a chatbot using NLTK](https://medium.com/analytics-vidhya/building-a-simple-chatbot-in-python-using-nltk-7c8c8215ac6e);

