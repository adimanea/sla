# Verificare formală și căutare de bug-uri

Proiectul meu va folosi limbajul [ACSL](https://frama-c.com/acsl.html) din cadrul pachetului Frama-C, pentru a verifica formal programe scrise în C.

Deocamdată, studiez [exemple](https://github.com/fraunhoferfokus/acsl-by-example/blob/master/ACSL-by-Example.pdf).
